import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Nvidia Geforce RTX 3090 vs. Cookies?', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('Let us take a quick look comparison at the 2 choices - '
                  'Cast your vote on which option you will choose!', style: TextStyle(
                  color: Colors.blue[500],),
                ),
              ],
            ),
          ),
          //Icon(Icons.check_box, color: Colors.blue[500],),
          //Text('84'),
        ],
      ),
    );

    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.green, Icons.videogame_asset, '3090'),
          _buildButtonColumn(Colors.amber, Icons.restaurant_menu, 'Cookies'),
          _buildButtonColumn(Colors.lightGreenAccent, Icons.add, 'Both!'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
          'Would you rather pay 1500 dollars for a Nvidia Geforce RTX 3090? '
              'Or, would you rather buy 1500 cookies for 1500 dollars? The decision '
              'is very difficult to make. The positives of getting the graphics '
              'card would be that you can have the most powerful gaming PC on '
              'the market. If you get cookies... well then you have 1500 '
              'cookies... that sounds amazing. The choice is yours!', style: TextStyle(
        color: Colors.deepPurple
        ),
      ),
    );
    return MaterialApp(
      title: 'text',
      home: Scaffold(
        appBar: AppBar(
          title: Text('CS481 HW2'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/3090.jpg',
              width: 600,
              height: 150,
              fit: BoxFit.cover,
            ),
            Image.asset(
              'images/cookies.jpg',
              width: 600,
              height: 150,
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSection,
            textSection
          ]
        ),
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}

